using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using FivePD.API;
using FivePD.API.Utils;

namespace WumpusCallouts
{

    [CalloutProperties("SuspiciousActivity (Prison)", "Wumpus", "0.0.1")]
    public class SuspiciousActivity : Callout
    {
        private Ped suspect, suspect1;

        public SuspiciousActivity()
        {
            Random random = new Random();
            int x = random.Next(1, 100 + 1);
            if (x <= 40)
            {
                InitInfo(new Vector3(1732.94f, 2504.92f, 45.56f));

            }
            else if (x >= 40 && x <= 60)
            {
                InitInfo(new Vector3(1770.89f, 2482.56f, 55.14f));
            }
            else if (x >= 60)
            {
                InitInfo(new Vector3(1746.77f, 2458.8f, 45.57f));
            }
            ShortName = "SuspiciousActivity (Prison)";
            CalloutDescription = "A suspicious group of prisoners.";
            ResponseCode = 3;
            StartDistance = 30f;
        }

        public async override Task OnAccept()
        {
            InitBlip();
            UpdateData();

            suspect = await SpawnPed(PedHash.Prisoner01, Location);
            suspect1 = await SpawnPed(PedHash.PrisMuscl01SMY, Location);

            suspect.BlockPermanentEvents = true;
            suspect.AlwaysKeepTask = true;
            suspect1.BlockPermanentEvents = false;
            suspect1.AlwaysKeepTask = true;
        }

        public override void OnStart(Ped player)
        {
            PedData data = new PedData();
            data.BloodAlcoholLevel = 0.00;
            List<Item> items = new List<Item>();
            Item Shiv = new Item
            {
                Name = "Shiv",
                IsIllegal = true
            };
            items.Add(Shiv);
            data.Items = items;
            Utilities.SetPedData(suspect1.NetworkId, data);

            PlayerData playerData = Utilities.GetPlayerData();
            string displayName = playerData.DisplayName;

            // var weapon = WeaponHash.Machete | WeaponHash.Bat | WeaponHash.Wrench;

            base.OnStart(player);
            DrawSubtitle("oh shit, cops.", 10000);
            // Simplest, but not thread safe
            Random s_Random = new Random();

            int perCent = s_Random.Next(0, 2);

            if (perCent == 1)               //  0 .. 49
            {
                Notify("~o~Officer ~b~" + displayName + ",~o~ a ped is fleeing!");
                suspect.Task.FleeFrom(player);
                suspect1.Task.FleeFrom(player);
                suspect.AttachBlip();
                suspect1.AttachBlip();
            }
            else if (perCent == 0)     // 50 .. 69
            {
                suspect.Weapons.Give(WeaponHash.Knife, 15, true, true);
                suspect.Task.FightAgainst(player);
                suspect1.Task.HandsUp(600000);
                suspect1.AttachBlip();
                // return Item of size 5
            }
            else if (perCent == 2)
            {
                suspect1.Weapons.Give(WeaponHash.KnuckleDuster, 15, true, true);
                suspect1.Task.FightAgainst(player);
                suspect.Task.HandsUp(600000);
                suspect.AttachBlip();
            }

        }

        private void Notify(string message)
        {
            API.BeginTextCommandThefeedPost("STRING");
            API.AddTextComponentSubstringPlayerName(message);
            API.EndTextCommandThefeedPostTicker(false, true);
        }
        private void DrawSubtitle(string message, int duration)
        {
            API.BeginTextCommandPrint("STRING");
            API.AddTextComponentSubstringPlayerName(message);
            API.EndTextCommandPrint(duration, false);
        }
    }
}