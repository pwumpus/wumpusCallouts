using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using FivePD.API;
using FivePD.API.Utils;

namespace WumpusCallouts
{

    [CalloutProperties("Prison Breach (Prison)", "Wumpus", "0.0.1")]
    public class PrisonBreak : Callout
    {
        public Ped suspect, suspect1, suspect2;
        
        public PrisonBreak()
        {
            Random random = new Random();
            int x = random.Next(1, 100 + 1);
            if (x <= 40)
            {
                InitInfo(new Vector3(1876.11f, 2604.98f, 45.67f));

            }
            else if (x >= 40 && x <= 60)
            {
                InitInfo(new Vector3(1876.11f, 2604.98f, 45.67f));
            }
            else if (x >= 60)
            {
                InitInfo(new Vector3(1876.11f, 2604.98f, 45.67f));
            }
            ShortName = "Prison Breach (Prison)";
            CalloutDescription = "A group of civilians are attempting a prison breach.";
            ResponseCode = 3;
            StartDistance = 300f;
        }

        public override async Task OnAccept()
        {
            Ped closest;
            InitBlip();
            UpdateData();

            var weapons = new[]
            {
                WeaponHash.CarbineRifleMk2,
                WeaponHash.HeavySniper,
                WeaponHash.SMG,
                WeaponHash.SpecialCarbine,
                WeaponHash.Musket,
                WeaponHash.APPistol,
                WeaponHash.Gusenberg,
            };

            suspect = await SpawnPed(RandomUtils.GetRandomPed(), Location);
            suspect1 = await SpawnPed(RandomUtils.GetRandomPed(), Location);
            suspect2 = await SpawnPed(RandomUtils.GetRandomPed(), Location);
            suspect.Weapons.Give(weapons[RandomUtils.Random.Next(weapons.Length)], int.MaxValue, true, true);
            suspect1.Weapons.Give(weapons[RandomUtils.Random.Next(weapons.Length)], int.MaxValue, true, true);
            suspect2.Weapons.Give(weapons[RandomUtils.Random.Next(weapons.Length)], int.MaxValue, true, true);
            suspect.Armor = 100;
            suspect1.Armor = 100;
            suspect2.Armor = 100;


            suspect.BlockPermanentEvents = true;
            suspect.AlwaysKeepTask = true;
            suspect1.BlockPermanentEvents = true;
            suspect1.AlwaysKeepTask = true;
            suspect2.BlockPermanentEvents = true;
            suspect2.AlwaysKeepTask = true;
            
            suspect.Task.RunTo(new Vector3(1722.81f,2532.72f,45.31f ));
            suspect1.Task.RunTo(new Vector3(1722.81f,2532.72f,45.31f ));
            suspect2.Task.RunTo(new Vector3(1722.81f,2532.72f,45.31f ));

        }

        public async override void OnStart(Ped closest)
        {
            base.OnStart(closest);
            suspect.AttachBlip();
            suspect1.AttachBlip();
            suspect2.AttachBlip();
            
            await API.Wait(60000);
            Tick += KillAll;
        }

        private async Task KillAll()
        {
            suspect.Task.ShootAt(Game.PlayerPed);
            suspect1.Task.ShootAt(Game.PlayerPed);
            suspect2.Task.ShootAt(Game.PlayerPed);
        }
    }
}