using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using FivePD.API;
using FivePD.API.Utils;

namespace WumpusCallouts
{

    [CalloutProperties("Indecent Exposure", "Wumpus", "0.0.1")]
    public class IndecentExposure : Callout
    {
        private Ped suspect;

        public IndecentExposure()
        {
            Random rnd = new Random();
            float offsetX = rnd.Next(100, 700);
            float offsetY = rnd.Next(100, 700);
            InitInfo(World.GetNextPositionOnStreet(Game.PlayerPed.GetOffsetPosition(new Vector3(offsetX, offsetY, 0))));
            ShortName = "Indecent Exposure";
            CalloutDescription = "A civilian reported a male ped showing genitiala.";
            ResponseCode = 3;
            StartDistance = 120f;
        }

        public async override Task OnAccept()
        {
            InitBlip();
            UpdateData();

            Random s2_Random = new Random();
            var ped = PedHash.Acult01AMY;
            var status = true;
            int vaLcent = s2_Random.Next(1, 4);
            if(vaLcent == 1)
            {
                ped = PedHash.Acult01AMY;
            }
            if (vaLcent == 2)
            {
                ped = PedHash.Acult02AMY;
            }
            if (vaLcent == 5)
            {
                ped = PedHash.Acult02AMY;
            }
            if (vaLcent == 6)
            {
                ped = PedHash.Acult01AMM;
            }
            if (vaLcent == 3)
            {
                ped = PedHash.Topless01AFY;
            }
            if (vaLcent == 4)
            {
                ped = PedHash.Stripper02SFY;
            }           
            if (vaLcent == 7)
            {
                ped = GetRandomPed()
                status = false;
            }            
            if (vaLcent == 8)
            {
                ped = GetRandomPed()
                status = false;
            }

            suspect = await SpawnPed(ped, Location);
            suspect.AlwaysKeepTask = false;
            suspect.BlockPermanentEvents = false;
            suspect.Task.WanderAround();
            Notify("This call may contain nudity, this may not be acceptable for some ages, or streaming platforms.");
        }

        public override void OnStart(Ped player)
        {
            PedData data = new PedData();
            data.BloodAlcoholLevel = 0.00;
            Utilities.SetPedData(suspect.NetworkId, data);
            PlayerData playerData = Utilities.GetPlayerData();
            string displayName = playerData.DisplayName;

            // var weapon = WeaponHash.Machete | WeaponHash.Bat | WeaponHash.Wrench;

            base.OnStart(player);
            suspect.AttachBlip();
            // Simplest, but not thread safe
            Random s_Random = new Random();

            int perCent = s_Random.Next(0, 2);

            if (perCent == 1)               //  0 .. 49
            {
                Notify("~o~Officer ~b~" + displayName + ",~o~ the ped is fleeing!");
                suspect.Task.FleeFrom(player);
                data.BloodAlcoholLevel = 0.89;
                API.SetPedIsDrunk(suspect.GetHashCode(), true);
            }
            else if (status == false)
            {
                Notify("~o~Officer ~b~" + displayName + ",~o~ The suspect may potentially not be nude.");
            }
            else if (perCent == 2)     // 50 .. 69
            {
                suspect.Task.WanderAround();
                Notify("~o~Officer ~b~" + displayName + ",~o~ the caller reported that the ped has a weapon!");
                suspect.Weapons.Give(WeaponHash.Bottle, 100, true, true);

                // return Item of size 5
            }
            else if(perCent == 0)
            {
                suspect.Task.WanderAround();
                data.BloodAlcoholLevel = 0.00;
            }

        }

        private void Notify(string message)
        {
            API.BeginTextCommandThefeedPost("STRING");
            API.AddTextComponentSubstringPlayerName(message);
            API.EndTextCommandThefeedPostTicker(false, true);
        }
        private void DrawSubtitle(string message, int duration)
        {
            API.BeginTextCommandPrint("STRING");
            API.AddTextComponentSubstringPlayerName(message);
            API.EndTextCommandPrint(duration, false);
        }
    }
}
