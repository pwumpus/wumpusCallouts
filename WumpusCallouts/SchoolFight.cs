﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using FivePD.API;
using FivePD.API.Utils;

namespace WumpusCallouts
{

    [CalloutProperties("Assualt On Campus", "Wumpus", "0.0.1")]
    public class SchoolFight : Callout
    {
        Ped suspect, suspect1, suspect2, suspect3, suspect4;

        public SchoolFight()
        {
            Random random = new Random();
            int x = random.Next(1, 100 + 1);
            if (x <= 40)
            {
                InitInfo(new Vector3(-1718.58f, 124.57f, 64.37f));

            }
            else if (x >= 40 && x <= 60)
            {
                InitInfo(new Vector3(-1609.14f, 264.15f, 59.55f));
            }
            else if (x >= 60)
            {
                InitInfo(new Vector3(-1621.13f, 201.44f, 60.48f));
            }
            ShortName = "School Fight";
            CalloutDescription = "A civilian reported a group of students fighting.";
            ResponseCode = 3;
            StartDistance = 120f;
        }

        public async override Task OnAccept()
        {
            InitBlip();
            UpdateData();

            suspect = await SpawnPed(PedHash.Eastsa01AMY, Location);
            suspect1 = await SpawnPed(PedHash.Eastsa02AMY, Location);
            suspect2 = await SpawnPed(PedHash.Beachvesp01AMY, Location);
            suspect3 = await SpawnPed(PedHash.HughCutscene, Location);
            suspect4 = await SpawnPed(PedHash.Ktown01AMY, Location);

            suspect.BlockPermanentEvents = true;
            suspect.AlwaysKeepTask = true;
            suspect1.BlockPermanentEvents = false;
            suspect1.AlwaysKeepTask = true;
            suspect2.BlockPermanentEvents = false;
            suspect2.AlwaysKeepTask = true;
            suspect3.BlockPermanentEvents = false;
            suspect3.AlwaysKeepTask = true;
            suspect4.BlockPermanentEvents = false;
            suspect4.AlwaysKeepTask = true;
        }

        public override void OnStart(Ped player)
        {

            PlayerData playerData = Utilities.GetPlayerData();
            string displayName = playerData.DisplayName;

            // var weapon = WeaponHash.Machete | WeaponHash.Bat | WeaponHash.Wrench;

            base.OnStart(player);
            DrawSubtitle("Punk!!", 10000);
            // Simplest, but not thread safe
            Random s_Random = new Random();

            int perCent = s_Random.Next(0, 2);

            if (perCent == 1)               //  0 .. 49
            {
                Notify("~o~Officer ~b~" + displayName + ",~o~ a ped is fleeing!");
                suspect4.Task.FleeFrom(player);
                suspect1.Task.FleeFrom(suspect4);
                suspect.Task.EnterAnyVehicle(VehicleSeat.Driver);
                suspect.AttachBlip();
            }
            else if (perCent == 0)     // 50 .. 69
            {
                suspect.Task.HandsUp(60000);
                Notify("~o~Officer ~b~" + displayName + ",~o~ the caller reported that a ped has a weapon!");
                suspect3.Weapons.Give(WeaponHash.Knife, 15, true, true);
                suspect2.Weapons.Give(WeaponHash.KnuckleDuster, 5, true, true);
                suspect4.Weapons.Give(WeaponHash.Unarmed, 5, true, true);
                suspect1.Weapons.Give(WeaponHash.Unarmed, 5, true, true);
                suspect1.Task.FightAgainst(suspect);
                suspect.Task.FightAgainst(suspect2);
                suspect3.Task.FightAgainst(suspect4);
                suspect4.Task.FightAgainst(suspect1);


                // return Item of size 5
            }
            else if (perCent == 2)
            {
                suspect.Task.HandsUp(60000);
                Notify("~o~Officer ~b~" + displayName + ",~o~ the caller reported that a ped has a weapon!");
                suspect3.Weapons.Give(WeaponHash.Knife, 15, true, true);
                suspect2.Weapons.Give(WeaponHash.Unarmed, 5, true, true);
                suspect4.Weapons.Give(WeaponHash.KnuckleDuster, 5, true, true);
                suspect4.Weapons.Give(WeaponHash.KnuckleDuster, 5, true, true);
                suspect1.Task.FightAgainst(suspect);
                suspect.Task.FightAgainst(suspect2);
                suspect3.Task.FightAgainst(suspect4);
                suspect4.Task.FightAgainst(suspect1);
            }

        }

        private void Notify(string message)
        {
            API.BeginTextCommandThefeedPost("STRING");
            API.AddTextComponentSubstringPlayerName(message);
            API.EndTextCommandThefeedPostTicker(false, true);
        }
        private void DrawSubtitle(string message, int duration)
        {
            API.BeginTextCommandPrint("STRING");
            API.AddTextComponentSubstringPlayerName(message);
            API.EndTextCommandPrint(duration, false);
        }
    }
}