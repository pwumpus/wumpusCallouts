﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using FivePD.API;
using FivePD.API.Utils;

namespace WumpusCallouts
{

    [CalloutProperties("Intoxicated Driver Pursuit", "Wumpus", "0.0.2")]
    public class DrunkDriver : Callout
    {

        private Vehicle car;
        private Ped driver;
        private string[] carList = { "adder", "bullet", "emerus", "gp1", "fmj", "le7b", "nero", "euros", "jester", "reaper", "zentorno" };

        public DrunkDriver()
        {
            Random rnd = new Random();
            float offsetX = rnd.Next(100, 700);
            float offsetY = rnd.Next(100, 700);
            InitInfo(World.GetNextPositionOnStreet(Game.PlayerPed.GetOffsetPosition(new Vector3(offsetX, offsetY, 0))));
            ShortName = "High Speed Pursuit";
            CalloutDescription = "A driver in a sports vehicle is drinking and driving.";
            ResponseCode = 3;
            StartDistance = 150f;
        }

        public async override void OnStart(Ped player)
        {
            base.OnStart(player);
            driver = await SpawnPed(RandomUtils.GetRandomPed(), Location + 2);
            Random random = new Random();
            string cartype = carList[random.Next(carList.Length)];
            VehicleHash Hash = (VehicleHash)API.GetHashKey(cartype);
            car = await SpawnVehicle(Hash, Location);
            driver.SetIntoVehicle(car, VehicleSeat.Driver);
            PlayerData playerData = Utilities.GetPlayerData();
            string displayName = playerData.DisplayName;
            Notify("The suspect was seen driving a " + cartype + ".");

            //Driver Data
            PedData data = new PedData();
            data.BloodAlcoholLevel = 2.77;
            List<Item> items = new List<Item>();
            Item BeerBottle = new Item
            {
                Name = "Beer",
                IsIllegal = false
            };
            items.Add(BeerBottle);
            data.Items = items;
            Utilities.SetPedData(driver.NetworkId, data);
            Utilities.ExcludeVehicleFromTrafficStop(car.NetworkId, true);

            //Tasks
            driver.AlwaysKeepTask = true;
            driver.BlockPermanentEvents = true;
            API.SetPedIsDrunk(driver.GetHashCode(), true);
            API.SetDriveTaskMaxCruiseSpeed(driver.GetHashCode(), 85f);
            API.SetDriveTaskDrivingStyle(driver.GetHashCode(), 16777216);
            driver.Task.FleeFrom(player);
            car.AttachBlip();
            driver.AttachBlip();
            PedData data1 = await Utilities.GetPedData(driver.NetworkId);
            string firstname = data1.FirstName;
        }

        public async override Task OnAccept()
        {
            InitBlip();
            UpdateData();
        }
        private void Notify(string message)
        {
            API.BeginTextCommandThefeedPost("STRING");
            API.AddTextComponentSubstringPlayerName(message);
            API.EndTextCommandThefeedPostTicker(false, true);
        }
        private void DrawSubtitle(string message, int duration)
        {
            API.BeginTextCommandPrint("STRING");
            API.AddTextComponentSubstringPlayerName(message);
            API.EndTextCommandPrint(duration, false);
        }
    }
}