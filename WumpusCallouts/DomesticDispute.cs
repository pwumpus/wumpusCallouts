﻿using System;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using FivePD.API;
using FivePD.API.Utils;
using System.Collections.Generic;

namespace WumpusCallouts
{

    [CalloutProperties("Domestic Dispute", "Wumpus", "0.0.1")]
    public class DomesticDispute : Callout
    {
        Ped suspect, victim;

        public DomesticDispute()
        {
            Random random = new Random();
            int x = random.Next(1, 100 + 1);
            if (x <= 40)
            {
                InitInfo(new Vector3(1877.76f, 3932.91f, 32.52f));

            }
            else if (x >= 40 && x <= 60)
            {
                InitInfo(new Vector3(894.7f, 3610.13f, 32.82f));
            }
            else if (x >= 60)
            {
                InitInfo(new Vector3(1395.89f, 3645.63f, 34.06f));
            }
            ShortName = "Domestic Dispute";
            CalloutDescription = "A civilian reported an a violent arguement between 2 indiviuals.";
            ResponseCode = 3;
            StartDistance = 120f;
        }

        public async override Task OnAccept()
        {
            InitBlip();
            UpdateData();
            suspect = await SpawnPed(RandomUtils.GetRandomPed(), Location);
            victim = await SpawnPed(RandomUtils.GetRandomPed(), Location);
            suspect.AlwaysKeepTask = true;
            suspect.BlockPermanentEvents = true;
            victim.AlwaysKeepTask = false;
            victim.BlockPermanentEvents = true;
        }

        public async override void OnStart(Ped player)
        {
            PedData data = new PedData();
            data.BloodAlcoholLevel = 0.89;
            List<Item> items = new List<Item>();
            Item Jerker = new Item
            {
                Name = "Portable Dick Jerker",
                IsIllegal = false
            };
            Item Cuffs = new Item
            {
                Name = "Handcuffs",
                IsIllegal = false
            };
            items.Add(Jerker);
            items.Add(Cuffs);
            data.Items = items;
            Utilities.SetPedData(suspect.NetworkId, data);            // var weapon = WeaponHash.Machete | WeaponHash.Bat | WeaponHash.Wrench;

            base.OnStart(player);
            suspect.Weapons.Give(WeaponHash.Unarmed, 1, true, true);
            suspect.Task.FightAgainst(victim);
            suspect.AttachBlip();
            API.SetPedIsDrunk(suspect.GetHashCode(), true);
            API.SetPedArmour(victim.GetHashCode(), 100);
            PedData data1 = await Utilities.GetPedData(suspect.NetworkId);
            DrawSubtitle("~r~[" + data1.FirstName + "] ~s~Suck my dick bitch!!", 5000);
            API.Wait(24000);
            suspect.Task.FleeFrom(player);
        }
        private void DrawSubtitle(string message, int duration)
        {
            API.BeginTextCommandPrint("STRING");
            API.AddTextComponentSubstringPlayerName(message);
            API.EndTextCommandPrint(duration, false);
        }
    }
}