﻿using System;
using System.Threading.Tasks;
using CitizenFX.Core;
using FivePD.API;
using FivePD.API.Utils;
using System.Linq;

namespace WumpusCallouts
{

    [CalloutProperties("Aggravated Assault & Battery", "Wumpus", "0.0.1")]
    public class AggravatedAB : Callout
    {
        Ped suspect, victim;

        public AggravatedAB()
        {
            Random random = new Random();
            int x = random.Next(1, 100 + 1);
            if (x <= 40)
            {
                InitInfo(new Vector3(1593.43f, 3585.98f, 33.77f));

            }
            else if (x >= 40 && x <= 60)
            {
                InitInfo(new Vector3(1546.22f, 3592.41f, 35.45f));
            }
            else if (x >= 60)
            {
                InitInfo(new Vector3(1995.96f, 3772.2f, 32.18f));
            }
            ShortName = "Aggravated Assault & Battery";
            CalloutDescription = "A civilian reported an Aggravated Assault & Battery in public.";
            ResponseCode = 3;
            StartDistance = 120f;
        }

        public async override Task OnAccept()
        {
            InitBlip();
            UpdateData();
            suspect = await SpawnPed(RandomUtils.GetRandomPed(), Location);
            victim = await SpawnPed(RandomUtils.GetRandomPed(), Location);
            suspect.AlwaysKeepTask = true;
            suspect.BlockPermanentEvents = true;
            victim.AlwaysKeepTask = true;
            victim.BlockPermanentEvents = true;
        }

        public override void OnStart(Ped player)
        {
            // var weapon = WeaponHash.Machete | WeaponHash.Bat | WeaponHash.Wrench;

            base.OnStart(player);
            suspect.Weapons.Give(RandomUtils.GetRandomWeapon(), 1, true, true);
            suspect.Task.FightAgainst(suspect);
            victim.Task.ReactAndFlee(suspect);
            suspect.AttachBlip();
            victim.AttachBlip();
        }
    }
}