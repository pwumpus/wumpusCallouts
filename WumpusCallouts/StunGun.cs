﻿using System;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using FivePD.API;
using FivePD.API.Utils;

namespace WumpusCallouts
{

    [CalloutProperties("Civilian w/ Stun Gun", "Wumpus", "0.0.1")]
    public class StunGun : Callout
    {
        Ped suspect, victim;

        public StunGun()
        {
            Random random = new Random();
            int x = random.Next(1, 100 + 1);
            if (x <= 40)
            {
                InitInfo(new Vector3(1074.96f, 3749.64f, 33.59f));

            }
            else if (x >= 40 && x <= 60)
            {
                InitInfo(new Vector3(1986.84f, 3031.26f, 46.62f));
            }
            else if (x >= 60)
            {
                InitInfo(new Vector3(920.51f, 3565.27f, 33.28f));
            }
            ShortName = "Civilian w/ Stun Gun";
            CalloutDescription = "A civilian reported an a ped with a stun gun.";
            ResponseCode = 3;
            StartDistance = 120f;
        }

        public async override Task OnAccept()
        {
            InitBlip();
            UpdateData();
            suspect = await SpawnPed(RandomUtils.GetRandomPed(), Location);
            victim = await SpawnPed(RandomUtils.GetRandomPed(), Location);
            suspect.AlwaysKeepTask = true;
            suspect.BlockPermanentEvents = true;
            victim.AlwaysKeepTask = true;
            victim.BlockPermanentEvents = true;
            Notify("This specific callout is still in beta, and may be subject to change, expect bugs. ");
        }

        public override void OnStart(Ped player)
        {
            // var weapon = WeaponHash.Machete | WeaponHash.Bat | WeaponHash.Wrench;

            base.OnStart(player);
            suspect.Weapons.Give(WeaponHash.StunGun, 2, true, true);
            suspect.Task.FightAgainst(victim);
            victim.Task.ReactAndFlee(suspect);
            suspect.AttachBlip();
            Notify("A bystander reported a victim being constantly stunned.");
            API.Wait(30000);
        }

        private void Notify(string message)
        {
            API.BeginTextCommandThefeedPost("STRING");
            API.AddTextComponentSubstringPlayerName(message);
            API.EndTextCommandThefeedPostTicker(false, true);
        }
    }
}