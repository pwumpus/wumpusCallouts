using System;
using System.Threading.Tasks;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using FivePD.API;


namespace WumpusCallouts
{
    [CalloutProperties("Individual Under The Influence", "Wumpus", "0.0.1")]
    public class DrunkPed : Callout
    {
        Ped suspect;

        public DrunkPed()
        {
            Random rnd = new Random();
            float offsetX = rnd.Next(100, 700);
            float offsetY = rnd.Next(100, 700);
            InitInfo(World.GetNextPositionOnStreet(Game.PlayerPed.GetOffsetPosition(new Vector3(offsetX, offsetY, 0))));
            ShortName = "Individual Under The Influence";
            CalloutDescription = "A male was spotted walking with his genitiala visible.";
            ResponseCode = 2;
            StartDistance = 150f;
        }

        public async override void OnStart(Ped player)
        {
            base.OnStart(player);
            suspect = await SpawnPed(PedHash.Acult01AMY, Location + 2);
            API.SetPedIsDrunk(suspect.GetHashCode(), true);
            API.SetPedScream(suspect.GetHashCode());
            API.SetPedArmour(suspect.GetHashCode(), 100);
            suspect.AlwaysKeepTask = true;
            suspect.BlockPermanentEvents = true;
            suspect.Weapons.Give(WeaponHash.Bottle, 100, true, true);
            suspect.AttachBlip();
            suspect.Task.FightAgainst(player);
        }

        public async override Task OnAccept()
        {
            InitBlip();
            UpdateData();
            Notify("This call may contain nudity, this may not be acceptable for some ages, or streaming platforms.");
        }

        private void Notify(string message)
        {
            API.BeginTextCommandThefeedPost("STRING");
            API.AddTextComponentSubstringPlayerName(message);
            API.EndTextCommandThefeedPostTicker(false, true);
        }
    }
}
